module.exports = function (RED) {
    function Tibbit28Node(config) {
        RED.nodes.createNode(this, config);
        var node = this;
        var now = new Date();
	var nowMillis = Date.UTC(now.getUTCFullYear(),now.getUTCMonth(),now.getUTCDate(),now.getUTCHours(),now.getUTCMinutes(), 1);
    var lib = require("../../@tibbo-tps/tibbit-28/build/Release/tibbit28.node");


        this.on('input', function (msg) {
        msg.topic = '{time:' + nowMillis/1000 + '; Slot:' + config.slot +'}';
        var light = new lib.Light();
		msg.payload = light.getIllumination(config.slot);
        node.send(msg);
        });
    }
    RED.nodes.registerType("tibbit-28", Tibbit28Node);
}