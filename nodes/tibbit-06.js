module.exports = function (RED) {
    function Tibbit06Node(config) {
        RED.nodes.createNode(this, config);
        var node = this;

        var lib = require("../lib/ltps_gpio.node");
        var gpio = new lib.Gpio();
        gpio.setDirection(config.slot + "A", 1); // 0 for "input", 1 for "output"
        gpio.setDirection(config.slot + "B", 1); // 0 for "input", 1 for "output"
        gpio.setValue(config.slot + "A", 1);
        gpio.setValue(config.slot + "B", 1);


        this.on('input', function (msg) {

            if ((msg.payload == 1 | msg.payload == 0) && (msg.topic == "A" | msg.topic == "B")) {
                gpio.setValue(config.slot + msg.topic, parseInt(msg.payload));
            }

            node.send(msg);
        });
    }

    RED.nodes.registerType("tibbit-06", Tibbit06Node);
}